//
//  ViewController.swift
//  PartyRock
//
//  Created by Visakeswaran N on 22/07/17.
//  Copyright © 2017 Visakeswaran N. All rights reserved.
//

import UIKit

class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var partyRocks = [PartyRock]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let p1 = PartyRock(imageURL: "https://i.scdn.co/image/66ff51342a9b250bf5b998fd0ec8e977671468bc", videoURL: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/eVTXPUF4Oz4\" frameborder=\"0\" allowfullscreen></iframe>", videoTitle: "IN THE END")
        
        let p2 = PartyRock(imageURL: "https://s-media-cache-ak0.pinimg.com/236x/4f/a5/f9/4fa5f982dee436ebb2d026ba95157452--liking-park-music-metal.jpg", videoURL: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Gd9OhYroLN0\" frameborder=\"0\" allowfullscreen></iframe>", videoTitle: "PAPERCUT")
        
        let p3 = PartyRock(imageURL: "https://c1.staticflickr.com/5/4059/4379355229_58fddd8353.jpg", videoURL: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/ScNNfyq3d_w?list=PLBiPNxqFKPZIWTooh9mCq3rIphK86Almm\" frameborder=\"0\" allowfullscreen></iframe>", videoTitle: "CRAWLING")
        
        let p4 = PartyRock(imageURL: "http://img08.deviantart.net/3385/i/2006/073/f/0/fake_linkin_park_album_cover_by_cadmiumred.png", videoURL: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/kXYiU_JCYtU\" frameborder=\"0\" allowfullscreen></iframe>", videoTitle: "NUMB")
        
        let p5 = PartyRock(imageURL: "https://upload.wikimedia.org/wikipedia/en/9/98/Linkin_Park_Burn_It_Down.jpg", videoURL: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/ysSxxIqKNN0\" frameborder=\"0\" allowfullscreen></iframe>", videoTitle: "NEW DIVIDE")
        
        partyRocks.append(p1)
        partyRocks.append(p2)
        partyRocks.append(p3)
        partyRocks.append(p4)
        partyRocks.append(p5)
        
        tableView.delegate = self
        tableView.dataSource = self
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PartyCell", for: indexPath) as? PartyCell{
            
            let partyRock = partyRocks[indexPath.row]
            cell.updateUI(partyRock: partyRock)
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return partyRocks.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let partyRock = partyRocks[indexPath.row]
        
        performSegue(withIdentifier: "VideoVC", sender: partyRock)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? VideoVC{
            if let party = sender as? PartyRock{
                destination.partyRock = party
            }
        }
    }
}

